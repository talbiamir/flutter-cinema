import 'dart:io';

import 'package:flutter/material.dart';
import 'package:projet/variables.dart';
import 'package:projet/api.dart';
import 'dart:developer';
import 'package:intl/intl.dart';

class Movie extends StatefulWidget {
  const Movie({super.key});

  @override
  State<Movie> createState() => _MovieState();
}

class _MovieState extends State<Movie> {
  @override
  Widget build(BuildContext context) {
    dynamic movie = Variables.movie;
    final String title = movie['title'].toString();
    final img = movie['backdrop_path'].toString();
    final vote = movie['vote_average'].toString();
    final avis = movie['vote_count'].toString();
    final description = movie['overview'].toString();
    final genres = movie['genres'];

    DateTime date = DateTime.parse(movie['release_date']);
    String formattedDate = DateFormat('dd/MM/yyyy').format(date);

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {},
          ),
        ],
      ),
      body: Card(
        child: Column(children: <Widget>[
          Image.network('https://image.tmdb.org/t/p/w500$img'),
          Container(
            padding: const EdgeInsets.only(top: 25, bottom: 25),
            child: Text(title,
                style: const TextStyle(fontSize: 24),
                textAlign: TextAlign.left),
          ),
          Text(description),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(top: 15, bottom: 25),
                child: Text('Date de sortie : $formattedDate'),
              ),
              const SizedBox(
                width: 10,
              ), // Espacement entre le texte et le reste de la colonne
            ],
          ),
        ]),
      ),
    );
  }
}
