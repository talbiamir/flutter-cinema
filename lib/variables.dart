class Variables {
  static List<dynamic> movies = [];
  static List<dynamic> genres = [];
  static List<dynamic> categories = [];
  static Map<String, dynamic> movie = {};
  static String apikey = "942c7fde2758aede907a00e9e1f37ab6";
  static int pages = 1;
  static int currentPage = 1;
  static String currentGenre = "popular";
  static String movieId = "";
}
