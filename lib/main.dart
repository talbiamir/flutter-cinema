import 'package:flutter/material.dart';
import 'variables.dart';
import 'api.dart';
import 'movies.dart';
import 'movie.dart';
import 'quizz.dart';

void main() async {
  Variables.movies = await Api.fetchMovies("popular", Variables.currentPage);
  Variables.categories = await Api.fetchCategories();
  Variables.genres = await Api.fetchGenres();

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: Movies(),
      initialRoute: '/',
      routes: {
        '/': (context) => const Movies(),
        '/page1': (context) => const Movie(),
        '/page2': (context) => QuizPageFlutter(),
      },
    );
  }
}
