import 'package:flutter/material.dart';

//Quiz Flutter
class QuizPageFlutter extends StatefulWidget {
  @override
  _QuizPageStateFlutter createState() => _QuizPageStateFlutter();
}

class _QuizPageStateFlutter extends State<QuizPageFlutter> {
  int _questionIndex = 0;
  int _score = 0;

  final List<Map<String, Object>> _questions = [
    {
      'question': 'Quel est le film qui a fait le plus d\'entrées ?',
      'answers': [
        {'text': 'Avatar', 'correct': true},
        {'text': 'Titanic', 'correct': false},
        {'text': 'Avengers : Endgame', 'correct': false},
        {'text': 'Spider-Man : No Way Home', 'correct': false},
      ],
    },
    {
      'question': 'Quel est le film qui a coûté le plus cher ?',
      'answers': [
        {'text': 'Avatar : La Voie de l\'eau', 'correct': false},
        {'text': 'Avengers : Endgame', 'correct': false},
        {
          'text': 'Pirates des Caraïbes : La Fontaine de Jouvence',
          'correct': true
        },
        {'text': 'Star Wars : L\'Ascension de Skywalker', 'correct': false},
      ],
    },
    {
      'question': 'Quelle est la durée moyenne d\'un film ?',
      'answers': [
        {'text': '95 minutes', 'correct': false},
        {'text': '105 minutes', 'correct': false},
        {'text': '115 minutes', 'correct': false},
        {'text': '125 minutes', 'correct': true},
      ],
    },
  ];

  void _answerQuestion(bool isCorrect) {
    setState(() {
      if (isCorrect) {
        _score++;
      }
      _questionIndex++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Quiz'),
        leading: IconButton(
          icon: Icon(Icons.home),
          onPressed: () {
            // Ici, on définit ce qui se passe lorsque l'utilisateur appuie sur le bouton de retour à la page d'accueil
            Navigator.popUntil(context, ModalRoute.withName('/'));
          },
        ),
      ),
      body: Center(
        child: _questionIndex < _questions.length
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _questions[_questionIndex]['question'].toString(),
                    style: const TextStyle(fontSize: 24),
                  ),
                  const SizedBox(height: 20),
                  ...(_questions[_questionIndex]['answers']
                          as List<Map<String, Object>>)
                      .map(
                        (answer) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Container(
                            width: 300,
                            height: 50,
                            child: ElevatedButton(
                              onPressed: () {
                                _answerQuestion(answer['correct'] as bool);
                              },
                              child: Text(answer['text'].toString()),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ],
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Vous avez terminé le quiz!',
                    style: TextStyle(fontSize: 24),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'Votre score: $_score/${_questions.length}',
                    style: const TextStyle(fontSize: 18),
                  ),
                  const SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/');
                    },
                    child: const Text('Retourner à l\'accueil'),
                  ),
                ],
              ),
      ),
    );
  }
}
