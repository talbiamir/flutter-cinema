import 'dart:async';
import 'dart:convert';
import 'variables.dart';

import 'package:http/http.dart' as http;

import 'dart:developer';

class Api {
  static Future<List<dynamic>> fetchMovies(categorie, currentPage) async {
    final String apikey = Variables.apikey;
    final String apiUrl =
        'https://api.themoviedb.org/3/movie/$categorie?api_key=$apikey&language=fr&page=$currentPage';

    final response = await http.get(Uri.parse(apiUrl));
    if (response.statusCode == 200) {
      final jsonData = jsonDecode(response.body);
      final List<dynamic> movies = jsonData['results'];
      return movies;
    } else {
      throw Exception('Failed to load movies');
    }
  }

  static Future<List<dynamic>> fetchGenres() async {
    final String apikey = Variables.apikey;
    final String apiUrl =
        'https://api.themoviedb.org/3/genre/movie/list?api_key=$apikey';

    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      final jsonData = jsonDecode(response.body);
      final List<dynamic> genres = jsonData['genres'];
      return genres;
    } else {
      throw Exception('Failed to load genres');
    }
  }

  static Future<List<dynamic>> fetchCategories() async {
    final String apikey = Variables.apikey;
    final String apiUrl =
        'https://api.themoviedb.org/3/discover/movie?api_key=$apikey';

    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      final jsonData = jsonDecode(response.body);
      final List<dynamic> categories = jsonData['results'];
      return categories;
    } else {
      throw Exception('Failed to load movies');
    }
  }
}
