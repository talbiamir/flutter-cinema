import 'package:flutter/material.dart';
import 'package:projet/movie.dart';
import 'package:projet/variables.dart';
import 'package:projet/api.dart';
import 'dart:developer';

class Movies extends StatefulWidget {
  const Movies({super.key});

  @override
  State<Movies> createState() => _MoviesState();
}

class _MoviesState extends State<Movies> {
  List<dynamic> _data = Variables.movies;
  // List<dynamic> _categories = Variables.categories;
  String currentCategory = "populaires";
  ScrollController scrollController = ScrollController();

  void _miseAJour() {
    setState(() {
      super.initState();
      _data = Variables.movies;
    });
  }

  @override
  void initState() {
    super.initState();
    scrollController.addListener(pagination);
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  void pagination() async {
    if (scrollController.offset >= scrollController.position.maxScrollExtent &&
        !scrollController.position.outOfRange) {
      Variables.currentPage += 1;
      List<dynamic> newData =
          await Api.fetchMovies(Variables.currentGenre, Variables.currentPage);
      setState(() {
        _data.addAll(newData);
      });
    }
  }

  Future<void> loadData() async {
    Variables.currentPage += 1;
    final List<dynamic> newData =
        await Api.fetchMovies(Variables.currentGenre, Variables.currentPage);
    setState(() {
      _data.addAll(newData);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Films $currentCategory'),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.quiz),
            onPressed: () {
              Navigator.pushNamed(context, '/page2');
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('Filtrer'),
            ),
            ListTile(
              title: const Text('Populaire'),
              onTap: () async {
                Variables.movies =
                    await Api.fetchMovies("popular", Variables.currentPage);
                currentCategory = "populaires";
                Variables.currentGenre = 'popular';
                _miseAJour();
              },
            ),
            ListTile(
              title: const Text('Mieux notés'),
              onTap: () async {
                Variables.movies =
                    await Api.fetchMovies("top_rated", Variables.currentPage);
                currentCategory = "les mieux notés";
                Variables.currentGenre = 'top_rated';
                _miseAJour();
              },
            ),
            ListTile(
              title: const Text('A venir'),
              onTap: () async {
                Variables.movies =
                    await Api.fetchMovies("upcoming", Variables.currentPage);
                currentCategory = "à venir";
                Variables.currentGenre = 'upcoming';
                _miseAJour();
              },
            ),
            ListTile(
              title: const Text('A l\'affiche'),
              onTap: () async {
                Variables.movies =
                    await Api.fetchMovies("now_playing", Variables.currentPage);
                currentCategory = "à l'affiche";
                Variables.currentGenre = 'now_playing';
                _miseAJour();
              },
            ),
          ],
        ),
      ),
      body: ListView.builder(
        controller: scrollController,
        itemCount: _data.length + 1,
        itemBuilder: (BuildContext context, int index) {
          if (index == _data.length) {
            _buildLoader();
          } else {
            final item = _data[index];
            final img = item['poster_path'].toString();
            final vote = item['vote_average'].toString();
            final avis = item['vote_count'].toString();
            final title = item['title'];
            return InkWell(
              onTap: () async {
                Variables.movie = item;
                Navigator.pushNamed(context, '/page1');
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Image.network('https://image.tmdb.org/t/p/w500$img'),
                    Text(title, style: const TextStyle(fontSize: 20)),
                    Container(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Text('$vote/10 via $avis avis',
                          textAlign: TextAlign.left),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildLoader() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
