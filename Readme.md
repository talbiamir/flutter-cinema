# Application de films en flutter

## Créée par Amir TALBI grâce à l'**api TMDB**

### Usage 
```
flutter run lib/main.dart
```

### Description
Créée dans le cadre d'un exercice, cette application permet de retrouver des films selon différents tris (popularité, note, etc). L'application possède une pagination automatique lorsque l'on arrive en bas de page. De plus, il y a un petit quiz de 3 questionsn que l'on trouve à droite de l'appbar.